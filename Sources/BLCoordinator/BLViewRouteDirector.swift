//
//  ViewRouteDirector.swift
//  
//
//  Created by Brian Lane on 1/5/20.
//

#if !os(macOS)
import UIKit

/// Allows view controllers to be shown or dismissed.
public protocol ViewRouteDirector: AnyObject {
  func present(_ viewController: UIViewController, animated: Bool)
  func present(_ viewController: UIViewController,
               animated: Bool,
               onDismissed: (()->Void)?)
  func dismiss(animated: Bool)
}

public extension ViewRouteDirector {
  public func present(_ viewController: UIViewController,
                      animated: Bool) {
    present(viewController, animated: animated, onDismissed: nil)
  }
}

#endif
