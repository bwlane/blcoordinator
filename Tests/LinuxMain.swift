import XCTest

import BLCoordinatorTests

var tests = [XCTestCaseEntry]()
tests += BLCoordinatorTests.allTests()
XCTMain(tests)
