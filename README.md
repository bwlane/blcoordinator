# BLCoordinator

This is a simple implementation of the Coordinator pattern as a Swift package. 

The pattern relies on three simple concepts. The first is that a ViewController implement a protocol called `StoryboardInstantiable`. This allows a quick way to reference storyboard files and use them to initialize views in code. 

`Coordinator` is a protocol that creates a hierarchical relationship between `ViewController`. A parent `Coordinator` will reference its child(ren) and will know how the relationship works. The `Coordinator` knows what relationship exists between two `ViewControllers`.

`ViewRouteDirector` allows the implementing class to know how to present other `ViewControllers` but does not know anything about the relationship with that `ViewController`. The `ViewRouteDirector` knows how to show or dismiss `ViewControllers`.

As an example, a `MainCoordinator` might reference its `ChildCoordinator`. When the user performs an action, say pressing a button on a `MainViewController`, that `MainViewController` passes the action to the `MainCoordinator`, which knows which `ChildCoordinator` to present. It parameterizes this information to the `MainViewRouteDirector`, which will then follow a stack pattern to push/pop the views onto a stack and contain information about how to present it.


### More Info
[How to use the coordinator pattern in iOS apps](https://www.hackingwithswift.com/articles/71/how-to-use-the-coordinator-pattern-in-ios-apps)

[Coordinators Redux](https://khanlou.com/2015/10/coordinators-redux/)
